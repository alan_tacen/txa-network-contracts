// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity ^0.8.19;

import "../BaseDeploy.sol";
import "../../../src/Manager/ProcessingChain/ProcessingChainManager.sol";
import "../../../src/Rollup/Rollup.sol";

/**
 * Used to claim trading fees in the Rollup contract.
 * Requires the following env vars:
 * - TRADE_PROOF: this is the hex-encoded data to prove that a trade exists in a state root.
 *   This data is provided by the validator.
 * - PRIVATE_KEY: private key for address set as participatingInterface in ProcessingChainManager
 */
contract ClaimTrade is BaseDeploy {
    using stdJson for string;

    function run() external {
        onlyOnProcessingChain();
        string memory json = vm.readFile(processingChainContractsPath);
        ProcessingChainManager manager = ProcessingChainManager(abi.decode(json.parseRaw(".manager"), (address)));
        Rollup rollup = Rollup(manager.rollup());

        bytes memory tradeProof = vm.envBytes("TRADE_PROOF");
        Rollup.TradeProof memory proof = abi.decode(tradeProof, (Rollup.TradeProof));
        Rollup.TradeProof[] memory proofs = new Rollup.TradeProof[](1);
        proofs[0] = proof;
        uint256 epoch = Id.unwrap(rollup.lastConfirmedEpoch());
        Rollup.TradingFeeClaim memory claim = Rollup.TradingFeeClaim(epoch, proofs);
        Rollup.TradingFeeClaim[] memory claims = new Rollup.TradingFeeClaim[](1);
        claims[0] = claim;
        
        vm.startBroadcast(vm.envUint("PRIVATE_KEY"));
        rollup.claimTradingFees(claims);
        vm.stopBroadcast();
    }
}
