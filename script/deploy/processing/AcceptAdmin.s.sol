// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../BaseDeploy.sol";
import "../../../src/Manager/ProcessingChain/ProcessingChainManager.sol";

// This script is called by an address that was designated as an admin to accept the transfer.
// Until the function is called, the old admin retains privileges.
//
// Require env vars:
// - PRIVATE_KEY: private key of account that has been designated as the new admin
contract AcceptAdmin is BaseDeploy {
    using stdJson for string;

    function run() external {
        onlyOnProcessingChain();
        string memory json = vm.readFile(processingChainContractsPath);
        ProcessingChainManager manager = ProcessingChainManager(abi.decode(json.parseRaw(".manager"), (address)));
        vm.startBroadcast(vm.envUint("PRIVATE_KEY"));
        manager.acceptAdminTransfer();
        vm.stopBroadcast();
    }
}
