#!/bin/bash
set -a
source ./script/.processing.mainnet.env
source ./script/.auth.mainnet.env
set +a

# Ensure all required environment variables are set
REQUIRED_VARS=("ARB_RPC_URL" "PRIVATE_KEY" "TRADE_PROOF")

for var in "${REQUIRED_VARS[@]}"; do
    if [ -z "${!var}" ]; then
        echo "Error: $var is not set!"
        exit 1
    fi
done

script_files=(
  "processing/ClaimTradingFees.s.sol"
)

rpc_urls=(
  $ARB_RPC_URL
)

auth_files=(
  "./script/.auth.mainnet.env"
)


export PRIVATE_KEY
export TRADE_PROOF

for index in ${!script_files[*]}; do
  script_file=${script_files[$index]}
  rpc_url=${rpc_urls[$index]}
  auth_file=${auth_files[$index]}
  source $auth_file
  forge script script/deploy/$script_file --rpc-url $rpc_url -vvvv

  if [ $? -eq 0 ]; then
    forge script script/deploy/$script_file --rpc-url $rpc_url --broadcast  --legacy --slow --gas-limit 100000000

    if [ $? -ne 0 ]; then
      echo "Broadcasting $script_file failed"
      exit 1
    fi
  else
    echo "Simulating $script_file failed"
    exit 1
  fi
done
