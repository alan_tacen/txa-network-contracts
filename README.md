# TXA Network Contracts

## Installation

This repository uses Foundry for smart contract development. Installation instructions and further information can be found in the [Foundry Book](https://book.getfoundry.sh/getting-started/installation)

This repository uses [Husky](https://typicode.github.io/husky/) for pre-commit hooks, which requires `npm`. It's recommend to use `nvm`:
```
nvm use --lts
```

## Compiling
```
forge build
```

## Testing
```
forge build
```

